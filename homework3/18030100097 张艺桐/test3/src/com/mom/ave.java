package com.mom;
import org.apache.activemq.ActiveMQConnectionFactory;
import javax.jms.*;
class MyListener_ave implements MessageListener {
    int num = 0;
    double sum = 0;
    @Override
    public void onMessage(Message message) {
        try {
            num++;
            String str = ((TextMessage)message).getText();
            sum += Double.valueOf(str);
            System.out.println("num: "+num+" average: "+sum/num);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
public class ave {
    public static void main(String[] args) throws JMSException {
        String brokerURL = "tcp://localhost:61616";
        ConnectionFactory factory = null;
        Connection connection = null;
        Session session = null;
        Topic topic = null;
        MessageConsumer messageConsumer = null;
        MyListener_ave listener = null;
        try {
            factory = new ActiveMQConnectionFactory(brokerURL);
            connection = factory.createConnection();

            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            topic = session.createTopic("MYTOPIC");

            messageConsumer = session.createConsumer(topic);

            listener = new MyListener_ave();

            messageConsumer.setMessageListener(listener);

            connection.start();

            System.out.println("exit.");
            System.in.read();   // Pause
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.close();
        }
    }

}
